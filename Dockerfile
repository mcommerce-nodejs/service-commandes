FROM node:20-alpine
ENV NODE_ENV=production
WORKDIR /app
COPY ["package.json", "package-lock.json", "./"]
RUN npm ci --only=production
USER node
COPY . .
EXPOSE 3001
CMD ["node", "server.js"]
